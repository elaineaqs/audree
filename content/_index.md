+++
title = "Home"
herotitle = "Welcome to the Homepage"
description = "Describe the Homepage right here"
author = "Hugo Authors"
menu = ""
weight = "-100"
+++

## Sample home content

Written in Go, Hugo is an open source static site generator available under the [Apache Licence 2.0.](https://github.com/gohugoio/hugo/blob/master/LICENSE) Hugo supports TOML, YAML and JSON data file types, Markdown and HTML content files and uses shortcodes to add rich content. Other notable features are taxonomies, multilingual mode, image processing, custom output formats, HTML/CSS/JS minification and support for Sass SCSS workflows.
