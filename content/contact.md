---
title: Contact
herotitle: 'Contact Hero'
slug: ''
date: '2019-06-18T22:47:50+08:00'
draft: false
image: ''
menu: ''
weight: -70
author: ''
description: 'Contact me'
---
<div class="field">
  <label class="label">Your Name *</label>
  <div class="field is-grouped">
    <div class="control is-expanded">
        <input class="input" type="text" placeholder="Text input">
        <p class="help">First Name</p>
    </div>
    <div class="control is-expanded">
        <input class="input" type="text" placeholder="Text input">
        <p class="help">Last Name</p>
    </div>
  </div>
</div>

<div class="field">
  <label class="label">Your Email Address *</label>
  <div class="control is-expanded">
    <input class="input" type="email" placeholder="e.g. alexsmith@gmail.com">
  </div>
</div>

<div class="field">
  <label class="label">Your Message *</label>
  <div class="control is-expanded">
    <textarea class="textarea" placeholder="Explain how we can help you"></textarea>
  </div>
</div>

<div class="field is-grouped is-grouped-centered">
  <p class="control">
    <a class="button is-primary">
      Submit
    </a>
  </p>
</div>